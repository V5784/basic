package com.basic.training.varador.lesson02;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public abstract class AbstractList<E> implements List<E> {

    private static final String METHOD_NAME = "Method name";
    private static final String LIST_ITERATOR = " listIterator";
    private static final String ADD_ALL = " addAll";

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException(METHOD_NAME + " lastIndexOf");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException(METHOD_NAME + LIST_ITERATOR);
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException(METHOD_NAME + LIST_ITERATOR);
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException(METHOD_NAME + LIST_ITERATOR);
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException(METHOD_NAME + " isEmpty");
    }

    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException(METHOD_NAME + ADD_ALL);
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException(METHOD_NAME + ADD_ALL);
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException(METHOD_NAME + " clear");
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException(METHOD_NAME + " retainAll");
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException(METHOD_NAME + " removeAll");
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException(METHOD_NAME + " containsAll");
    }

    @Override
    public Object[] toArray(Object[] a) {
        throw new UnsupportedOperationException(METHOD_NAME + " toArray");
    }
}