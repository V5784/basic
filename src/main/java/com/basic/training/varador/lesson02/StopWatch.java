package com.basic.training.varador.lesson01;

import static java.lang.System.currentTimeMillis;

public class StopWatch {

    private long timeStart;

    public void start() {
        timeStart = currentTimeMillis();
    }

    public long getElapsedTime() {
        return currentTimeMillis() - timeStart;
    }
}

