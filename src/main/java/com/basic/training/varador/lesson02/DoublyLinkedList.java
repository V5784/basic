package com.basic.training.varador.lesson02;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<E> extends AbstractList<E> {

    private Node<E> head;
    private Node<E> tail;

    private int pointer;

    private class Node<T> {

        private T value;
        private Node<T> previous;
        private Node<T> next;

        private Node(Node<T> previous, T value, Node<T> next) {
            this.next = next;
            this.value = value;
            this.previous = previous;
        }
    }

    @Override
    public int size() {
        return pointer;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    public Object[] toArray() {

        Object[] result = new Object[pointer];

        for (int i = 0; i < pointer; i++) {
            result[i] = node(i).value;
        }
        return result;
    }

    @Override
    public boolean add(E element) {
        linkLast(element);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            for (int i = 0; i < pointer; i++) {
                if (node(i) == null) {
                    remove(i);
                    return true;
                }
            }
        } else {
            remove(indexOf(o));
            return true;
        }
        return false;
    }

    @Override
    public E get(int index) {

        rangeCheck(index);
        return node(index).value;
    }

    @Override
    public E set(int index, E element) {

        rangeCheckForAdd(index);

        Node<E> temp = node(index);
        E oldValue = temp.value;
        temp.value = element;
        return oldValue;
    }

    @Override
    public void add(int index, E element) {
        rangeCheckForAdd(index);

        if (index == pointer) {
            add(element);
        } else {
            linkBefore(index, element);
        }
    }

    private void linkBefore(int index, E element) {

        final Node<E> previous = node(index).previous;
        final Node<E> newNode = new Node<>(previous, element, node(index));

        if (previous == null) {
            head = newNode;
        } else {
            previous.next = newNode;
        }
        pointer++;
    }

    private void linkLast(E element) {
        final Node<E> last = tail;
        final Node<E> newNode = new Node<>(last, element, null);
        tail = newNode;

        if (last == null) {
            head = newNode;
        } else {
            last.next = newNode;
        }
        pointer++;
    }

    @Override
    public E remove(int index) {
        rangeCheck(index);
        return unlink(node(index));
    }

    private E unlink(Node<E> nodeForDelete) {
        final E value = nodeForDelete.value;
        final Node<E> previous = nodeForDelete.previous;
        final Node<E> next = nodeForDelete.next;

        if (previous == null) {
            head = next;
        } else {
            previous.next = next;
            nodeForDelete.previous = null;
        }
        if (next == null) {
            tail = previous;
        } else {
            next.previous = previous;
            nodeForDelete.next = null;
        }

        nodeForDelete.value = null;
        pointer--;
        return value;
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int index = 0; index < pointer; index++) {
                if (node(index).value == null) {
                    return index;
                }
            }
        }
        for (int index = 0; index < pointer; index++) {
            if (node(index).value.equals(o)) {
                return index;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return Arrays.toString(toArray());
    }

    private Node<E> node(int index) {
        if (index <= (pointer >> 1)) {
            Node<E> temp = head;
            for (int i = 0; i < index; i++) {
                temp = temp.next;
            }
            return temp;
        } else {
            Node<E> temp = tail;
            for (int i = pointer - 1; i > index; i--)
                temp = temp.previous;
            return temp;
        }
    }

    private void rangeCheckForAdd(int index) {
        if (index >= pointer || index < 0)
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private void rangeCheck(int index) {
        if (index >= pointer) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size();
    }

    @Override
    public Iterator<E> iterator() {
        return new ListIteratorImpl();
    }

    private class ListIteratorImpl implements Iterator<E> {

        int index = 0;
        int lastIndex = size();
        int lastRet = -1;

        @Override
        public boolean hasNext() {
            return index != size();
        }

        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            int i = index;
            if (i >= pointer) {
                throw new NoSuchElementException();
            }
            E next = get(i);
            lastRet = i;
            index++;
            return next;
        }

        @Override
        public void remove() {
            try {
                DoublyLinkedList.this.remove(lastRet);
                if (lastRet < lastIndex)
                    index--;
                lastRet = -1;
            } catch (IndexOutOfBoundsException e) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
