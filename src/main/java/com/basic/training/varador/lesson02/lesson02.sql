/*9 Найдите производителей ПК с процессором не менее 450 Мгц. Вывести: Maker */

SELECT DISTINCT Maker
FROM Product
  RIGHT JOIN PC
    ON Product.model = PC.model
WHERE speed >= 450;

/*10 Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price*/

SELECT
  Model,
  price
FROM Printer
WHERE price = (SELECT MAX(price)
               FROM Printer);

/*11 Найдите среднюю скорость ПК. */

SELECT AVG(speed) AS AVG_Speed
FROM PC;

/*12 Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол. */

SELECT AVG(speed) AS AVG_Speed
FROM Laptop
WHERE price > 1000;

/* 13 Найдите среднюю скорость ПК, выпущенных производителем A. */

SELECT AVG(speed) AS AVG_Speed
FROM PC
  LEFT JOIN Product
    ON PC.model = Product.Model
WHERE maker = 'A';

/*14 Найти производителей, которые выпускают более одной модели, при этом все выпускаемые производителем модели
являются продуктами одного типа. Вывести: maker, TYPE */

SELECT DISTINCT
  maker,
  type
FROM Product
WHERE maker IN (SELECT maker
                FROM Product
                GROUP BY maker
                HAVING COUNT(DISTINCT type) = 1 AND COUNT(model) > 1);

/*15 Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD */

SELECT HD
FROM PC
GROUP BY HD
HAVING COUNT(HD) >= 2;

/*16 Найдите пары моделей PC, имеющих одинаковые скорость и RAM. В результате каждая пара указывается только один раз,
т.е. (i,j), но не (j,i), Порядок вывода: модель с большим номером
 */
SELECT DISTINCT
  A.model AS model,
  B.model AS model,
  A.speed,
  A.ram
FROM PC AS A, PC B
WHERE A.speed = B.speed AND A.ram = B.ram AND
      A.model > B.model;

/* DML ____________________*/
/*4 Для каждой группы блокнотов с одинаковым номером модели добавить запись в таблицу PC со следующими характеристиками:
код: минимальный код блокнота в группе +20;
модель: номер модели блокнота +1000;
скорость: максимальная скорость блокнота в группе;
ram: максимальный объем ram блокнота в группе *2;
hd: максимальный объем hd блокнота в группе *2;
cd: значение по умолчанию;
цена: максимальная цена блокнота в группе, уменьшенная в 1,5 раза.
Замечание. Считать номер модели числом. */
INSERT INTO PC (code, model, speed, ram, hd, price)
  SELECT
    MIN(code) + 20,
    model + 1000,
    MAX(speed),
    MAX(ram) * 2,
    MAX(hd) * 2,
    MAX(price) / 1.5
  FROM laptop
  GROUP BY model;
/*5 Удалить из таблицы PC компьютеры, имеющие минимальный объем диска или памяти. */

DELETE FROM PC
WHERE HD <= (SELECT MIN(HD) FROM PC) OR RAM <= (SELECT MIN(RAM) FROM PC);

/*6 Удалить все блокноты, выпускаемые производителями, которые не выпускают принтеры. */

DELETE FROM laptop
WHERE model NOT IN (select model from Product where maker in (select maker from product where type ='Printer'));