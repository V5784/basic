package com.basic.training.varador.lesson02;

import com.basic.training.varador.lesson01.StopWatch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class DoubleLinkedListPerformanceTest {


    private static final int AMOUNT_NUMBERS_TO_ADD = 200_000;
    private static final int AMOUNT_NUMBERS_TO_DEL = 20_000;

    private static final String NEW_LINE = "\r\n";
    private static final String CHAR_ELEMENT = "(e) \t";
    private static final String MS = " ms";

    private static StopWatch myStopWatch = new StopWatch();
    private static StringBuilder result = new StringBuilder();

    private static List<Integer> myDll = new DoublyLinkedList<>();
    private static List<Integer> myDA = new DynamicArray<>();


    public static void main(String[] args) {

        File file = new File("src\\main\\java\\com\\basic\\training\\varador\\lesson02\\dllResult.txt");

        result.append(Values.operationAdd.getText());
        addToList(AMOUNT_NUMBERS_TO_ADD, myDll);
        addToList(AMOUNT_NUMBERS_TO_ADD, myDA);

        result.append(Values.operationDelFromStart.getText());
        removeFromList(0, AMOUNT_NUMBERS_TO_DEL, myDll);
        removeFromList(0, AMOUNT_NUMBERS_TO_DEL, myDA);

        result.append(Values.operationDelFromCenter.getText());
        removeFromList(myDll.size() >> 1, AMOUNT_NUMBERS_TO_DEL, myDll);
        removeFromList(myDA.size() >> 1, AMOUNT_NUMBERS_TO_DEL, myDA);

        result.append(Values.operationDelFromEnd.getText());
        removeFromList(myDll.size() - 1, AMOUNT_NUMBERS_TO_DEL, myDll);
        removeFromList(myDA.size() - 1, AMOUNT_NUMBERS_TO_DEL, myDA);

        try (FileWriter fw = new FileWriter(file)) {
            fw.write(result.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void addToList(int amount, List<Integer> list) {
        myStopWatch.start();
        for (int i = 0; i < amount; i++) {
            list.add(i);
        }
        myStopWatch.getElapsedTime();
        result.append(NEW_LINE).append(list.getClass().getSimpleName()).append(CHAR_ELEMENT).append(myStopWatch.getElapsedTime()).append(MS);
    }

    private static void removeFromList(int position, int amount, List<Integer> list) {
        myStopWatch.start();
        if (position == 0) {
            for (int i = 0; i < amount; i++) {
                list.remove(0);
            }
        } else {
            for (int i = 0; i < amount; i++) {
                list.remove(position);
                position--;
            }
        }
        myStopWatch.getElapsedTime();
        result.append(NEW_LINE).append(list.getClass().getSimpleName()).append(CHAR_ELEMENT).append(myStopWatch.getElapsedTime()).append(MS);
    }
}