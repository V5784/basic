package com.basic.training.varador.lesson02;


import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static java.lang.System.arraycopy;

public class DynamicArray<E> extends AbstractList<E> {

    private static final int DEFAULT_CAPACITY = 10;

    private Object[] array;

    private int pointer;

    public DynamicArray() {
        this(DEFAULT_CAPACITY);
    }

    public DynamicArray(int initialCapacity) {
        if (initialCapacity > 0) {
            this.array = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            this.array = new Object[0];
        } else {
            throw new IllegalArgumentException("Illegal argument of initialCapacity " + initialCapacity);
        }
    }

    private Object[] grow(int minCapacity) {
        array = Arrays.copyOf(array, newCapacity(minCapacity));
        return array;
    }

    private Object[] grow() {
        return grow(pointer + 1);
    }

    private int newCapacity(int minCapacity) {
        int oldCapacity = array.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        if (newCapacity - minCapacity <= 0) {
            if (array.length == 0) {
                newCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
                return newCapacity;
            }
            if (minCapacity < 0) // overflow
                throw new OutOfMemoryError();
            return minCapacity;
        }
        return newCapacity;
    }

    @Override
    public boolean add(Object e) {
        if (pointer == array.length) {
            array = grow();
        }
        array[pointer] = e;
        pointer++;
        return true;
    }

    @Override
    public void add(int index, Object element) {

        rangeCheckForAdd(index);

        if (pointer == array.length) {
            array = grow();
        }
        arraycopy(array, index, array, index + 1, pointer - index);
        array[index] = element;
        pointer++;
    }

    @Override
    public int size() {
        return pointer;
    }

    @Override
    public E set(int i, E e) {
        rangeCheckForAdd(i);
        E oldValue = elementData(i);
        array[i] = e;
        return oldValue;
    }

    @Override
    public E get(int i) {
        rangeCheck(i);
        return elementData(i);
    }

    @Override
    public E remove(int i) {
        rangeCheck(i);
        E oldValue = elementData(i);
        int amountElementsAfterIndex = size() - i - 1;
        if (amountElementsAfterIndex > 0) {
            arraycopy(array, i + 1, array, i, amountElementsAfterIndex);
        }
        --pointer;
        array[pointer] = null;
        return oldValue;
    }

    @Override
    public boolean remove(Object e) {
        if (e == null) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] == null) {
                    remove(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < array.length; i++) {
                if (e.equals(array[i])) {
                    remove(i);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int indexOf(Object e) {
        for (int i = 0; i < array.length; i++) {
            if (e.equals(array[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(array, pointer);
    }

    @SuppressWarnings("unchecked")
    private E elementData(int index) {
        return (E) array[index];
    }

    private void rangeCheck(int index) {
        if (index >= pointer) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private void rangeCheckForAdd(int index) {
        if (index > pointer || index < 0)
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size();
    }

    @Override
    public String toString() {
        return Arrays.toString(toArray());
    }

    @Override
    public ListIteratorImpl iterator() {
        return new ListIteratorImpl();
    }

    private class ListIteratorImpl implements Iterator<E> {
        int index = 0;
        int lastIndex = size();
        int lastRet = -1;

        @Override
        public boolean hasNext() {
            return index != size();
        }

        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            int i = index;
            E next = get(i);
            lastRet = i;
            index++;
            return next;
        }

        public boolean hasPrevious() {
            return lastIndex != 0;
        }

        public E previous() {
            int i = lastIndex - 1;
            E previous = get(i);
            lastRet = lastIndex = i;
            return previous;
        }

        public int nextIndex() {
            return index;
        }

        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            try {
                DynamicArray.this.remove(lastRet);
                if (lastRet < lastIndex)
                    index--;
                lastRet = -1;
            } catch (IndexOutOfBoundsException e) {
                throw new ConcurrentModificationException();
            }
        }

        public void set(E e) {
            DynamicArray.this.set(lastRet, e);
        }

        public void add(E e) {
            try {
                int i = index;
                DynamicArray.this.add(i, e);
                lastRet = -1;
                index = i + 1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
    }
}