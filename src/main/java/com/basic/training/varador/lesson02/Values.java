package com.basic.training.varador.lesson02;

public enum Values {

    operationAdd("\r\n-- -- -- -- Addition to the end -- -- -- --"),
    operationDelFromStart("\r\n-- -- -- -- Delete from start-- -- -- --"),
    operationDelFromCenter("\r\n -- -- -- -- Delete from center -- -- -- --"),
    operationDelFromEnd("\r\n-- -- -- -- Delete from end -- -- -- --");

    private String text;

    public String getText() {
        return text;
    }

    Values(String text) {
        this.text = text;
    }
}
