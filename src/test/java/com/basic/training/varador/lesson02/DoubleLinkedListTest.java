package com.basic.training.varador.lesson02;


import org.junit.Before;

import java.util.List;

public class DoubleLinkedListTest extends ListTest {

    private List<Person> arrayOfPersons = new DoublyLinkedList<>();
    private List<Person> dataList = new DoublyLinkedList<>();

    @Before
    @Override
    public void initializeList() {
        populateList();
        super.setListForTesting(arrayOfPersons);
        super.setDataList(dataList);
    }

    private void populateList() {
        for (int i = 1; i < 11; i++) {
            arrayOfPersons.add(new Person(i, "Person" + i));
        }
        for (int i = 20; i < 25; i++) {
            dataList.add(new Person(i, "Person" + i));
        }
    }
}