package com.basic.training.varador.lesson02;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

public class ListTest {

    private List<Object> listForTesting;
    private List<Object> dataList;


    public void setDataList(List dataList) {
        this.dataList = dataList;
    }

    public void setListForTesting(List listForTesting) {
        this.listForTesting = listForTesting;
    }

    @Before
    public void initializeList() {
    }

    @Test
    public void sizeCheck() {
        assertEquals(10, listForTesting.size());
    }

    @Test
    public void testGetElement() {
        Iterator<Object> iterator = listForTesting.iterator();
        Object o1 = listForTesting.get(0);
        assertEquals(o1, iterator.next());
        Object o2 = listForTesting.get(1);
        assertEquals(o2, iterator.next());
    }

    @Test
    public void testAdd() {
        Object o1 = dataList.get(0);
        assertTrue(listForTesting.add(o1));
        assertEquals(11, listForTesting.size());
        assertTrue(listForTesting.contains(o1));
    }

    @Test
    public void testAddByPosition() {
        Object o1 = dataList.get(1);
        listForTesting.add(1, o1);
        assertTrue(listForTesting.get(1).equals(o1));
        assertEquals(11, listForTesting.size());
    }

    @Test
    public void testSet() {
        Object o1 = dataList.get(3);
        listForTesting.set(0, o1);
        assertTrue(listForTesting.get(0).equals(o1));
        assertEquals(10, listForTesting.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testIndexOutOfBound() {
        listForTesting.set(10, 1);
    }

    @Test
    public void testRemove() {
        listForTesting.remove(1);
        assertEquals(9, listForTesting.size());
    }

    @Test
    public void testRemoveByObject() {
        Object o1 = dataList.get(2);
        listForTesting.add(o1);
        assertTrue(listForTesting.contains(o1));
        listForTesting.remove(o1);
        assertFalse(listForTesting.contains(o1));
        assertEquals(10, listForTesting.size());
    }

    @Test
    public void testLastIndexOf() {
        try {
            listForTesting.lastIndexOf(new Person(1, "person1"));
            fail("This is UnsupportedOperationException for DynamicArray");
        } catch (UnsupportedOperationException e) {
            assertEquals("Method name lastIndexOf", e.getMessage());
        } catch (Exception general) {
            fail("Unknown exception " + general.getClass().getSimpleName() + ", message " + general.getMessage());
        }
    }
}