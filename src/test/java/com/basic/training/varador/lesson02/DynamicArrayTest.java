package com.basic.training.varador.lesson02;


import org.junit.Before;

import java.util.List;

public class DynamicArrayTest extends ListTest {

    private List<Integer> arrayOfNumbers = new DynamicArray<>();
    private List<Integer> numbersForTest = new DynamicArray<>();

    @Before
    @Override
    public void initializeList() {
        populateList();
        super.setListForTesting(arrayOfNumbers);
        super.setDataList(numbersForTest);
    }

    private void populateList() {
        for (int i = 1; i < 11; i++) {
            arrayOfNumbers.add(i);
        }
        for (int i = 20; i < 26; i++) {
            numbersForTest.add(i);
        }
    }
}